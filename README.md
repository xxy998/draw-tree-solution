# draw-tree-solution

![screen-capture](docs/screen-capture.gif)

# Development

Build server:

```
npm run build
```

You can also run `npm run build -- --watch` to start TSC in watch mode.

Build site:

```
npm run webpack
```

You can set `NODE_ENV` to production to get an optimized webpack build as well:

```
NODE_ENV=production npm run webpack
```

or run webpack in watch mode during development:

```
npm run webpack -- --watch
```

Migrate data into Hypertrie:

```
npm run migrate
```

Start server:

```
npm run start
```

Visit http://localhost:3000

Linting:

```
npm run lint
```
