import createElement from 'virtual-dom/create-element'
import { NodeDeclaration } from './graph'

const h = require('virtual-hyperscript-svg')

function getDepth(t: NodeDeclaration, n: number, index: any) {
  if (!t) {
    return 0
  }
  const deps = t.parent ? [t.parent] : []
  let max = n
  for (let i = 0; i < deps.length; i++) {
    const dt = getDepth(index[deps[i]], n, index)
    if (dt > max) {
      max = dt
    }
  }
  return max + 1
}

function coords(sorted: Array<NodeDeclaration>, index: any) {
  const c: any = {}
  sorted.forEach((t, ix) => {
    if (!t) {
      return
    }
    const depth = getDepth(t, 0, index)
    const x1 = depth * 65
    const x0 = x1 - 65 + 5
    const y0 = (50 + 5) * ix
    const y1 = y0 + 50
    c[t.name] = [x0, y0, x1, y1]
  })
  return c
}

function createTreeSVG(sorted: Array<NodeDeclaration>) {
  const index = sorted.reduce<Record<string, NodeDeclaration>>((acc, x) => {
    acc[x.name] = x
    return acc
  }, {})
  const cix = coords(sorted, index)

  const groups = sorted.reverse().map((t) => {
    if (!t) {
      return ''
    }
    const c = cix[t.name]

    const x0 = 0,
      x1 = c[2] - c[0]
    const y0 = 0,
      y1 = c[3] - c[1]

    let pminy = c[1]
    ;([t.parent] || []).forEach((k) => {
      if (!cix[k]) {
        return
      }
      if (cix[k][3] < pminy) {
        pminy = cix[k][3]
      }
    })

    const arrowline = [
      [x0 - 25, pminy - c[1]],
      [x0 - 25, y0 + 25],
      [x0 - 15, y0 + 25],
    ]
    const arrowhead = [
      [x0 - 15, +20],
      [x0 - 15, y0 + 30],
      [x0 - 5, y0 + 25],
    ]

    const children = []
    children.push(
      h('rect', {
        fill: 'transparent',
        stroke: '#265d5d',
        'stroke-width': 2,
        x: x0,
        y: y0,
        width: x1 - x0,
        height: y1 - y0,
        dataset: {
          name: t.name,
          description: t.description,
        },
      })
    )
    children.push(
      h(
        'text',
        {
          x: 5,
          y: (y1 - y0 + 20 / 2) / 2,
          fill: '#265d5d',
        },
        t.name
      )
    )

    if (t.parent && t.parent.length) {
      children.push(
        h('polyline', {
          stroke: '#265d5d',
          'stroke-width': 2,
          fill: 'transparent',
          points: arrowline.join(' '),
        })
      )
      children.push(
        h('polygon', {
          fill: '#265d5d',
          points: arrowhead.join(' '),
        })
      )
    }
    return h(
      'g',
      {
        transform: 'translate(' + c[0] + ',' + c[1] + ')',
      },
      children
    )
  })

  return h('svg', { width: '100%', height: '100%' }, groups)
}

if (window.document && window.fetch) {
  const el = document.querySelector('#tree')
  fetch('http://localhost:3000/data').then((response) => {
    response.json().then((ret) => {
      el?.appendChild(createElement(createTreeSVG(ret)))

      const rects = document.querySelectorAll('rect')

      let currentTarget: any = null

      const disableCurrentTarget = () => {
        if (currentTarget) {
          currentTarget.style.stroke = '#265d5d'
          currentTarget.style.strokeWidth = 2
        }
        currentTarget = null
      }
      const addEventListeners = () => {
        const closeEl = document.querySelector('#close_box')
        const boxEl: HTMLDivElement | null = document.querySelector('#box')
        const titleEl: HTMLHeadingElement | null = document.querySelector('#box > h2')
        const descriptionEl: HTMLParagraphElement | null = document.querySelector('#box > p')

        if (!(closeEl && boxEl && titleEl && descriptionEl)) {
          return
        }

        closeEl.addEventListener('click', (e) => {
          e.preventDefault()
          boxEl.style.display = 'none'
          disableCurrentTarget()
        })

        rects.forEach((rect) =>
          rect.addEventListener('click', (e) => {
            disableCurrentTarget()
            currentTarget = e.target
            currentTarget.style.stroke = 'orange'
            currentTarget.style.strokeWidth = 4

            boxEl.style.display = 'block'

            titleEl.innerText = currentTarget.dataset.name
            descriptionEl.innerText = currentTarget.dataset.description
          })
        )
      }
      addEventListeners()
    })
  })
}
