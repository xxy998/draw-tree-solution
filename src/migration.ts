const hypertrie = require('hypertrie')

const data = [
  {
    name: 'A',
    description: 'This is a description of A',
    parentId: '',
  },
  {
    name: 'B',
    description: 'This is a description of B',
    parent: 'A',
  },
  {
    name: 'C',
    description: 'This is a description of C',
    parent: 'A',
  },
  {
    name: 'D',
    description: 'This is a description of D',
    parent: 'A',
  },
  {
    name: 'B-1',
    description: 'This is a description of B-1',
    parent: 'B',
  },
  {
    name: 'B-2',
    description: 'This is a description of B-2',
    parent: 'B',
  },
  {
    name: 'B-3',
    description: 'This is a description of B-3',
    parent: 'B',
  },
]

const DATA_DIR = __dirname + '/../tree.db'

const index = data.reduce<any>((acc, x) => {
  acc[x.name] = x
  return acc
}, {})

const records = data.map((d) => {
  const { description, name } = d
  const parent = d.parent || ''
  const keyParts = [name]
  if (parent) {
    const parents = []
    let p = parent
    let n
    while ((n = index[p]) !== undefined) {
      p = n.parent
      parents.push(n.name)
    }
    keyParts.push(...parents)
  }
  return {
    key: keyParts.reverse().join('/'),
    value: description,
    type: 'put',
  }
})

const db = hypertrie(DATA_DIR, { valueEncoding: 'json' })

db.batch(records, (er: Error | null, nodes: Array<any>) => {
  if (er) {
    throw er
  }
  console.log(`Inserted ${nodes.length} nodes`)
})
