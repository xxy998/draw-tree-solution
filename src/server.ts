import express from 'express'
import { tsort, getNodeDeclarationMap, createGraph } from './graph'

const DATA_DIR = __dirname + '/../tree.db'

const hypertrie = require('hypertrie')

const db = hypertrie(DATA_DIR, { valueEncoding: 'json' })

const app = express()
  .use(express.static(__dirname + '/../public'))
  .get('/data', (_, response) =>
    db.list((er: Error | null, nodes: Array<any>) => {
      if (er || !nodes.length) {
        return response.status(500).end()
      }

      const declarations = nodes.map((node) => {
        const { key, value } = node
        const parts = key.split('/')
        const prefix = parts.slice(0, -1)
        let parent = ''
        if (prefix.length) {
          parent = prefix.slice(-1)[0].toUpperCase()
        }
        return {
          name: parts.slice(-1)[0].toUpperCase(),
          parent,
          description: value,
        }
      })

      const sorted = tsort(createGraph(declarations)).sort()

      const map = getNodeDeclarationMap(declarations)

      response.json(sorted.map((name) => map[name]))
    })
  )

app.listen(3000)
