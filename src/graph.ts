export class Vertex {
  public afters: Array<string> = []
  constructor(public id: string) {}
}

export type Graph = { [key: string]: Vertex }

/** topological sort */
export function tsort(graph: Graph): Array<string> {
  const sorted: Array<string> = []
  const visited: { [key: string]: true } = {}
  const recursive: { [key: string]: true } = {}

  Object.keys(graph).forEach(function visit(id, ancestors: any) {
    if (visited[id]) {
      return
    }
    if (!graph[id]) {
      return
    }

    const vertex = graph[id]

    if (!Array.isArray(ancestors)) {
      ancestors = []
    }

    ancestors.push(id)
    visited[id] = true

    vertex.afters.forEach((afterId) => {
      if (ancestors.indexOf(afterId) >= 0) {
        recursive[id] = true
        recursive[afterId] = true
      } else {
        visit(afterId, ancestors.slice())
      }
    })

    sorted.unshift(id)
  })

  return sorted.filter((id) => !recursive[id])
}

export interface NodeDeclaration {
  name: string
  description: string
  parent: string
}

export function createGraph(ds: Array<NodeDeclaration>): Graph {
  const graph: Graph = {}
  ds.forEach((d) => {
    const name = d.name
    if (graph[name]) {
      throw new Error(`duplicated name: ${JSON.stringify(name)}`)
    }
    const vertex = (graph[name] = new Vertex(name))
    vertex.afters.push(...[d.parent])
  })
  return graph
}

export function getNodeDeclarationMap(declarations: Array<NodeDeclaration>): { [key: string]: NodeDeclaration } {
  const map: { [key: string]: NodeDeclaration } = {}
  declarations.forEach((d) => {
    map[d.name] = d
  })
  return map
}
